/* Objects:
    Syntax:
        let objectName = {
        		keyA: valueA, 
        		keyB: valueB
        }
*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
}

console.log('Result from creating objects using initializers/literal notation')
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);

// Creating objects using constructor function
/*
	- creates a reusable function to create several objects that have the same data structure
	Syntax:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}
	- "this" keyword allows to assign a new object's properties by associating them with values received from a constructors function's parameters 
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating objects using object constructors:');
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Result from creating objects using object constructors:');
console.log(myLaptop);

// Accessing Object Properties - 2 WAYS
// dot notation
console.log("Result from dot notation:" + myLaptop.name); // this one easier to use

// square bracket notation
console.log("Result from dot notation:" + myLaptop['name']); // it might get confusing

// Accessing array objects - nilagay sa isang array yung 2 properties

let arr = [laptop, myLaptop];

console.log(arr[0]['name']);
console.log(arr[0].name);

// Initializing/Adding/Deleting/Reassigning Object Properties

let car = {}

car.name = "Honda Civic";
console.log('Result from adding properties using dot notation');
console.log(car);

car['manufacture date'] = 2019; //ang isang pro nito is pwede gumamit ng spaces may space yung manufacture at date
console.log(car['manufacture date']);
console.log(car['Manufacture Date']); // case sensitive sila kaya undefined ang lumabas
console.log(car.manufactureDate); // CASE SENSITIVE KAYA DI LUMALABAS
console.log(car);

// Deleting object properties
delete car ['manufacture date'];
console.log('Result from deleting properties:');
console.log(car);

// Reassigning object properties
car.name = 'E46 BMW M3 GTR'; // ito na ang bagong value for the property name
console.log('Result from reassigning properties:');
console.log(car);

// Object Method
// Method is a function which is a property of an object (FUNCTION NA GINAWANG PROPERTY NG OBJECT T___T)

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name); //gamitin yung this. property para maaaccess yung property
	}
}

console.log(person);
console.log('Result from object methods');
person.talk();

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.')
}
person.walk();

// Add a run method to the person object
person.run = function(){
	console.log(this.name + ' ran 2 kilometers to see you.')
}
person.run();

// Creating reusable functions
let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas',
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
	}
}
friend.introduce();
console.log(friend.address);
console.log(friend.emails[1]);

























